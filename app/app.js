'use strict';

/**
 * App dependencies
 */
import * as angular from "angular";
import uirouter from 'angular-ui-router';
import notification from 'angular-ui-notification';
import angularLoadingBar from 'angular-loading-bar';
import 'ngMap';
import angularParallax from 'angular-parallax-npm';
import angularScrollAnimate from 'angular-scroll-animate';
import ngFileUpload from 'ng-file-upload';
import ngFileModel from 'ng-file-model';
import angularSocialLogin from 'angularjs-social-login';
import angularSocialShare from 'angular-socialshare';
import slickCarousel from 'slick-carousel';
import AgnularslickCarousel from 'angular-slick-carousel';
import angularFancyboxPlus from 'angular-fancybox-plus/js/angular-fancybox-plus.js';
import vcRecaptcha from 'angular-recaptcha';
import angularDisqusComments from 'angular-disqus-comments';
import ngMeta from 'ng-meta/dist/ngMeta.js';

/**
 * App config
 */
import run from 'core/app.run';
import config from 'core/app.config';
import routes from 'core/app.routes';

/**
 * App components
 */
import home from 'components/home';
// import termsOfUse from 'components/termsOfUse';
import contacts from 'components/contacts';
// import team from 'components/team';
import careers from 'components/careers';
import blog from 'components/blog';
import about from 'components/about';
import services from 'components/services';

/**
 * App shared components
 */
import {headerConfig} from 'components/shared/components/header';
import {footerConfig} from 'components/shared/components/footer';
import {imgAsyncConfig} from 'components/shared/components/img-async';
import {contactModalConfig} from 'components/shared/components/contact-modal';
import {requestModalConfig} from 'components/shared/components/request-modal';
import {preloaderConfig} from 'components/shared/components/preloader';

/**
 * App shared resources
 */
import wpDataResConfig from 'components/shared/resources/wpDataRes';

/**
 * App shared filters
 */
import sceTrustAsHtmlConfig from 'components/shared/filters/sceTrustAsHtml';
import htmlToPlaintextConfig from 'components/shared/filters/htmlToPlaintext';


/**
 * App bootstraping
 */
angular
	.module('mplify', [
		uirouter,
		notification,
        'ngMap',
        angularLoadingBar,
        'angular-parallax',
        'angular-scroll-animate',
        ngFileUpload,
		'ng-file-model',
        'socialLogin',
        angularSocialShare,
        'slickCarousel',
        'fancyboxplus',
        vcRecaptcha,
        angularDisqusComments,
        'ngMeta',

        home,
        careers,
        blog,
        contacts,
        about,
        services
	])
	.config(config)
	.config(routes)
	.run(run)
	.component('headerComponent', headerConfig)
	.component('footerComponent', footerConfig)
	.component('contactModal', contactModalConfig)
	.component('requestModal', requestModalConfig)
	.component('preloader', preloaderConfig)
	/**
	 * This one is a directive not a component as we want to
	 * avoid extra component wrapper element
	 */
	.directive('imgAsyncComponent', imgAsyncConfig)
	.service('wpDataRes', wpDataResConfig)
	.filter('sceTrustAsHtml', sceTrustAsHtmlConfig)
	.filter('htmlToPlaintext', htmlToPlaintextConfig)

