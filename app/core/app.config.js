'use strict';

import {CONST} from 'core/app.constants';

export default function appConfig($locationProvider, $provide, NotificationProvider, socialProvider, disqusCommentsProvider) {
    // $locationProvider.hashPrefix('!');

    /**
     * Provider to access temporary state in ui-router state resolve
     * Uses $transitions instead of $rootScope.$on('$stateChangeStart'...)
     * as those events are deprecated already
     */
    $provide.decorator('$state', function($delegate, $transitions) {
        $transitions.onBefore({
            to: '*.**'
        }, trans => {
            $delegate.next = trans.to();
        });
        return $delegate;
    });

    /**
     * Angular ui-notification provider 
     */
    NotificationProvider.setOptions({
        delay: 3000,
        replaceMessage: true,
        startTop: 20,
        startRight: 20,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top'
    });

    socialProvider.setGoogleKey(CONST.SOCIAL_PROVIDERS.GOOGLE.ID);
    socialProvider.setFbKey({appId: CONST.SOCIAL_PROVIDERS.FACEBOOK.ID, apiVersion: CONST.SOCIAL_PROVIDERS.FACEBOOK.VERSION});

    disqusCommentsProvider.shortName = CONST.DISQUS_SHORTNAME;
}