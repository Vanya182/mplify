export const envProd = {
    "ENV": {
        /**
         * WORDPRESS API
         *
         * Global WP Options
         */
        "API_BASE": "backend/wp-json",

        /**
         * WP data
         */
        "API": "/backend/wp-json/wp/v2"
    },

    "GOOGLE_MAP_KEY": "AIzaSyCNEVmkU3ImU4GsmdntMx1ZumHp_l22KQE",

    "WP": {
        "HOME_PAGE_ID": 4,
        "CAREERS_PAGE_ID": 22,
        "BLOG_PAGE_ID": 45,
        "CONTACT_US_PAGE_ID": 84,
        "ABOUT_US_PAGE_ID": 88,
        "SERVICES_PAGE_ID": 90
    },

    "STATIC": {
        "defaultThumb": "/app/assets/img/default-thumb.png"
    },

    "ADMIN_EMAIL": {
        "CONTACT": "contact@mplifytech.com",
        "JOB": "recruitment@mplifytech.com"
    },

    // https://elasticemail.com
    "ELASTIC_EMAIL": {
        "API": "https://api.elasticemail.com/v2/email/send",
        "API_KEY": "e0cc513e-6624-45a0-abee-a631d647e3a8",
        "TEMPLATES": {
            "CONTACT_FORM_ID": 3238, // Template which admin recieves
            "USER_JOB_APPLY_RESPONSE_ID": 3236, // Template which admin recieves
            "USER_JOB_APPLY_ID": 3223 // Template which admin recieves
        }
    },

    "DISQUS_SHORTNAME": "https-mplifytech-com",

    "GOOGLE_RECAPTCHA_KEY": "6LdHSkEUAAAAAOzAhtsI8J1Y6EDShSvoyXVk2N8e",
    "GOOGLE_MAP_KEY": "AIzaSyCXr8wbxf5hEqDRIMQKDSDPw26sR5vJkzw",

    "SOCIAL_PROVIDERS": {
        "GOOGLE": {
            "ID": "708045979215-m801dnr3gkcitcdo7e2r83f2jbceo69p.apps.googleusercontent.com"
        },
        "FACEBOOK": {
            "ID": "876284882550258",
            "VERSION": "v2.11"
        }
    }
}