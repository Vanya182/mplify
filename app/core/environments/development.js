export const envDev = {
    "ENV": {
        /**
         * WORDPRESS API
         *
         * Global WP Options
         */
        "API_BASE": "backend/wp-json",

        /**
         * WP data
         */
        "API": "/backend/wp-json/wp/v2"
    },

    "GOOGLE_MAP_KEY": "AIzaSyCNEVmkU3ImU4GsmdntMx1ZumHp_l22KQE",

    "WP": {
        "HOME_PAGE_ID": 4,
        "CAREERS_PAGE_ID": 22,
        "BLOG_PAGE_ID": 45,
        "CONTACT_US_PAGE_ID": 84,
        "ABOUT_US_PAGE_ID": 88,
        "SERVICES_PAGE_ID": 90
    },

    "STATIC": {
        "defaultThumb": "/app/assets/img/default-thumb.png"
    },

    "ADMIN_EMAIL": "vanyatsybulin@gmail.com",

    // https://elasticemail.com
    "ELASTIC_EMAIL": {
        "API": "https://api.elasticemail.com/v2/email/send",
        "API_KEY": "020b2211-f473-4c84-9077-daadecad038b",
        "TEMPLATES": {
            "CONTACT_FORM_ID": 139, // Template which admin recieves
            "USER_JOB_APPLY_RESPONSE_ID": 149, // Template which admin recieves
            "USER_JOB_APPLY_ID": 214 // Template which admin recieves
        }
    },

    "DISQUS_SHORTNAME": "mplify",

    "GOOGLE_RECAPTCHA_KEY": "6Lf-wT4UAAAAAIm8-Q00gbhyWKs6kuzl6XIEZ-Gg",
    "GOOGLE_MAP_KEY": "AIzaSyCXr8wbxf5hEqDRIMQKDSDPw26sR5vJkzw",

    "SOCIAL_PROVIDERS": {
        "GOOGLE": {
            "ID": "608676349423-9q838sj93thfl7ekrunn1i0dv9os3850.apps.googleusercontent.com",
            "SECRET": "R19xoxuzheXijOtmhA5Q0Zhi"
        },
        "FACEBOOK": {
            "ID": "1361611220633951",
            "VERSION": "v2.11"
        }
    }
}