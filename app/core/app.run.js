'use strict';

export default function run($rootScope, $state, $stateParams, $transitions, ngMeta) {
	$rootScope.$state = $state;
  	$rootScope.$stateParams = $stateParams;

  	$transitions.onEnter({ to: '*.**' }, function(event, toUrl, fromUrl) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
	});

    ngMeta.init();
}