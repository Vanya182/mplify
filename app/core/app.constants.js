'use strict';

import { envDev } from './environments/development.js'
import { envProd } from './environments/production.js'

const isProductionEnvironment = true;

export const CONST = isProductionEnvironment ? envProd : envDev;