'use strict';

export default function routes($urlRouterProvider) {
	/**
	 * If the path doesn't match any of the urls you configured
	 * otherwise will take care of routing the user to the
	 * specified url
	 */
	$urlRouterProvider.otherwise('/');
}