'use strict'

import {CONST} from 'core/app.constants';


export default class wpDataRes {
	constructor($http, $sce, $filter){
		this._$http = $http;
		this._$sce = $sce;
		this._$filter = $filter;
	}	

	getData(id) {
		return this._$http.get(`${CONST.ENV.API}/pages/${id}`).then(res => {
			return res.data;
		}).catch(err => {
			console.error(err);
		});
	}

	getFooter() {
        return this._$http.get(`${CONST.ENV.API}/site_setting`).then(res => {
            return res.data;
        }).catch(err => {
            console.error(err);
        });
	}

}

