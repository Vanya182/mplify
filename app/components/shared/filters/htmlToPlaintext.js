'use strict';

export default function htmlToPlaintext() {
    return function(text) {
        return angular.element(text).text();
    }
}


