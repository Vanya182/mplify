'use strict';

import footerController from './footer.controller';
import template from './footer.html';

export const footerConfig = {
    template: template,
    controller: footerController,
    controllerAs: 'footerCtrl'
};

   
