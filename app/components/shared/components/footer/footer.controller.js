'use strict'

import {CONST} from 'core/app.constants';

export default class footerController {
    constructor($http, $scope, $state, $timeout, $transitions, $rootScope, Notification, wpDataRes) {
        this.CONST = CONST;
        this._$http = $http;
        this._$scope = $scope;
        this._$state = $state;
        this._$transitions = $transitions;

        this._$timeout = $timeout;
        this._$rootScope = $rootScope;
        this.Notification = Notification;

        this.footerData = null;
        this.footerData = wpDataRes.getFooter().then(data => {
            this.footerData = {
                info: data[0].acf.footer_,
                links: data[0].acf.social_links
            }
        }).catch(err => {
        });

        this.emailFormatRegex = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        this.date = new Date().getFullYear();
        this.form = {
            merge_name: null,
            merge_email: null,
            merge_phone: null,
            merge_message: null
        }

        this.isFormSubmitted = false;

        this._$transitions.onEnter({to: '*.**'}, trans => {
            /**
             * Reset for every state
             */
            this.currentStateName = null;
            this._$timeout(() => {
                this._$scope.$apply();
            })

            this._$scope.$watch(() => this._$state.current.name, (newValue, oldValue) => {
                if (newValue) {
                    this.currentStateName = newValue
                }
            });
        });
    }


    openContactModal() {
        this._$rootScope.$broadcast('toggleContactModal');
    }

    resetUserActions() {
        this.isFormSubmitted = false;
        this.myRecaptchaResponse = null;
        this.contactForm.$setPristine();
        this.form = {
            merge_name: null,
            merge_email: null,
            merge_phone: null,
            merge_message: null
        };
    }

    sendEmail() {
        this.isFormSubmitted = true;

        if (this.contactForm.$invalid || !this.myRecaptchaResponse) {
            return false;
        }

        let emailConfig = {
            apikey: CONST.ELASTIC_EMAIL.API_KEY,
            template: CONST.ELASTIC_EMAIL.TEMPLATES.CONTACT_FORM_ID,
            to: CONST.ADMIN_EMAIL.CONTACT,
        };

        let emailData = Object.assign(emailConfig, this.form)

        this._$http(
            {
                method: 'POST',
                url: CONST.ELASTIC_EMAIL.API,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: obj => {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: emailData
            }
        ).then(res => {
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.success('You have successfully sent message');
        }).catch(err => {
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.error(err.data || 'Error occured');
        })

    }
}
