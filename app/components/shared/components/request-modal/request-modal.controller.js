'use strict'

export default class modalController {
  constructor($rootScope) {
    this._$rootScope = $rootScope;
    this.body = angular.element('body');
    this.blurWrap = angular.element('.page-wrap');
    this.modal = angular.element('request-modal');
    this.modalIsActive = false;

    this._$rootScope.$on('toggleRequestModal', () => {
      this.toggleModal();
    });
  }

  toggleModal(){
    this.modalIsActive = !this.modalIsActive;
    this.body.toggleClass('no-overflow');
    this.blurWrap.toggleClass('blured');
  }
}