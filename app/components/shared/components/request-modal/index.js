'use strict';

import modalController from './request-modal.controller';
import template from './request-modal.html';

export const requestModalConfig = {
    template: template,
    controller: modalController,
    controllerAs: 'modalCtrl'
};

   
