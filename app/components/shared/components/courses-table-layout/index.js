'use strict';

import coursesTableLayoutController from './courses-table-layout.controller';
import template from './courses-table-layout.html';

export const coursesTableLayoutConfig = {
    template: template,
    bindings: {
    	route: '@',
    	color: '@'
    },
    controller: coursesTableLayoutController,
    controllerAs: 'coursesTableLayoutCtrl'
};

   
   
