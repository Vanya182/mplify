'use strict'

import routesRes from 'components/shared/resources/routesRes';

export default class coursesTableLayoutController {
  constructor(courseRes) {
    /**
     * Should use $onInit lifecycle hook to be sure all internal
     * stuff were done with element. Go to Component API docs for info
     */
    this.$onInit = ()=>{
      this.model = {
        courses: []
      }
      this.routeID = this.route;
      this.routeColorScheme = this.color;
      this.courseRes = courseRes;
      this.getCourses();
    }
  }

  getCourses(){
  	this.courseRes.getCoursesByParentID(this.routeID).then(data => {
      this.model.courses = data;
    }).catch(err => {
      console.error(err);
    });
  }
}