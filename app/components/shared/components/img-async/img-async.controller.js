import {CONST} from 'core/app.constants';

export default function imgAsyncController($element, $attrs, $http, $scope) {
	this._$element = $element;
	this._$attrs = $attrs;
	this._$scope = $scope;
  	this._$http = $http;
  	this.imgSrc = null;
  	/**
  	 * On this state all bindings are resolved
  	 */
  	this.$onInit = ()=>{
  		let mediaID = this._$scope.media;
		if (mediaID){
			this._$http.get(`${CONST.ENV.API}/media/${mediaID}`)
			.then(res => {
				this._$attrs.$set('src', res.data.source_url);
		    	this._$element.bind('error', function() {
	                if (this._$attrs.src != this._$attrs.errSrc) {
	                    /**
	                     * If error occured while loading media
	                     */
	                    this._$attrs.$set('src', CONST.STATIC.defaultThumb);
	                }
                });
		    })
		    .catch(err => {
		    	console.error(err);
		    });
		}else{
            this._$attrs.$set('src', CONST.STATIC.defaultThumb);
		}
  	}
}