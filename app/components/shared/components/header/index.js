'use strict';

import headerController from './header.controller';
import template from './header.html';

export const headerConfig = {
    template: template,
    controller: headerController,
    controllerAs: 'headerCtrl',
    bindings: {
        backgroundImage: '&'
    }
};

   
