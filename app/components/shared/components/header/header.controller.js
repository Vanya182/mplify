'use strict';

export default class headerController {
    constructor($scope, $rootScope, $transitions, $state, $timeout) {
        this._$scope = $scope;
        this._$rootScope = $rootScope;
        this._$transitions = $transitions;
        this._$state = $state;
        this._$timeout = $timeout;

        this.body = angular.element('body');
        this.blurWrap = angular.element('header');
        this.mobileNavActive = false;

        this.background = '/app/assets/img/photos/1.jpg';

        this._$transitions.onEnter( { to: '*.**' }, trans => {

            /**
             * Reset for every state
             */
            this.currentStateName = null;
        	this.pageTitle = null;

        	this._$timeout(() => {
        	    this._$scope.$apply();
            })

            this._$scope.$watch(() => this._$state.current.name, (newValue, oldValue) => {
                if (newValue){
                    this.currentStateName = newValue
                }
            });
            this._$scope.$watch(() => this._$state.current.pageTitle, (newValue, oldValue) => {
                if (newValue){
                    this.pageTitle = newValue;
                }
            });

        });

        this._$rootScope.$on('changeHeaderBackground', (event, args) => {
            this.background = args.background ? args.background : '/app/assets/img/photos/1.jpg';
            this._$timeout(() => {
                this._$scope.$apply();
            })
        });

        $(document).on('click touchstart', event => {
            if (this.mobileNavActive &&
                ! $(event.target).parents('.open-mobile-nav').length &&
                ! $(event.target).hasClass('menu-wr') &&
                ! $(event.target).parents('.menu-wr').length){
                this.openMobileNav();
            }
        });
    }

    uiSrefWasClicked() {
        if (this.mobileNavActive)
            this.openMobileNav();
    }

    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };

    openContactModal() {
        this.uiSrefWasClicked();
        this._$rootScope.$broadcast('toggleContactModal');
    }

    openMobileNav($event) {
        if ($event) {
            $event.stopPropagation();
        }
        this.triggerActiveStates();
        this._$timeout(() => {
            this._$scope.$apply();
        })
    }


    triggerActiveStates() {
        this.body.toggleClass('no-overflow');
        this.mobileNavActive = ! this.mobileNavActive;
        angular.element('header').toggleClass('blured');
        angular.element('.lines-set').toggleClass('blured');
        angular.element('.ui-view-page').toggleClass('blured');
    }
}