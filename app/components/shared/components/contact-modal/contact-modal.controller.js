'use strict'

import {CONST} from 'core/app.constants';

export default class modalController {
  constructor($scope, $rootScope, $http, Notification) {
    this._$scope = $scope;
    this._$rootScope = $rootScope;
    this._$http = $http;
    this.Notification = Notification;
    this.emailFormatRegex = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    this.body = angular.element('body');
    this.blurWrap = angular.element('.page-wrap');
    this.modal = angular.element('contact-modal');
    this.modalIsActive = false;

    this.form = {
        merge_name: null,
        merge_email: null,
        merge_phone: null,
        merge_message: null
    };

    this._$rootScope.$on('toggleContactModal', () => {
      this.toggleModal();
    });
  }


  toggleModal(){
    this.modalIsActive = !this.modalIsActive;
    this.body.toggleClass('no-overflow');
    this.blurWrap.toggleClass('blured');
  }

    resetUserActions() {
        this._$scope.contactForm.$setPristine();
        this.form = {
            merge_name: null,
            merge_email: null,
            merge_phone: null,
            merge_message: null
        };
    }

  send(){
      let emailConfig = {
          apikey: CONST.ELASTIC_EMAIL.API_KEY,
          template: CONST.ELASTIC_EMAIL.TEMPLATES.CONTACT_FORM_ID,
          to: CONST.ADMIN_EMAIL,
      };

      let emailData = Object.assign(emailConfig, this.form);

      this._$http(
          {
              method: 'POST',
              url: CONST.ELASTIC_EMAIL.API,
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              transformRequest: obj => {
                  var str = [];
                  for(var p in obj)
                      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                  return str.join("&");
              },
              data: emailData
          }
      ).then(res => {
          this.toggleModal();
            this.Notification.success('You have successfully sent message');
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
        }).catch(err => {
          this.resetUserActions();
          this._$timeout(() => {
              this._$scope.$apply();
          });
          this.Notification.error(err.data || 'Error occured');
        });
  }
}