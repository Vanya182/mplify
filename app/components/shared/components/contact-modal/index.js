'use strict';

import modalController from './contact-modal.controller';
import template from './contact-modal.html';

export const contactModalConfig = {
    template: template,
    controller: modalController,
    controllerAs: 'modalCtrl'
};

   
