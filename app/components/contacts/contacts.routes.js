'use strict';

import template from './contacts.html';
import {CONST} from 'core/app.constants';

export default function routes($stateProvider) {
  $stateProvider
    .state('contacts', {
  		url: '/contacts',
      	template: template,
      	controller: 'contactsController',
      	controllerAs: 'contactsCtrl',
        resolve: {
            pageData: ($state, $stateParams, wpDataRes)=>{
				let id = $stateParams.id;
				return wpDataRes.getData(CONST.WP.CONTACT_US_PAGE_ID).then(data => {
					/**
					 * Some adjusment for state data
					 * to use in page header
					 */
                    $state.next.pageTitle = data.title.rendered;
					return data;
				}).catch(err => {
				});
        	},
            data: (ngMeta, pageData) => {
                ngMeta.setTitle(pageData.yoast.title || pageData.title.rendered);
                ngMeta.setTag('description', pageData.yoast.metadesc);
            }
  		},
        meta: {
            disableUpdate: true
        }
    });
}
