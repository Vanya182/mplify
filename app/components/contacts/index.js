import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './contacts.routes';
import contactsController from './contacts.controller';

export default angular.module('app.contacts', [uirouter])
	.config(routing)
	.controller('contactsController', contactsController)
	// We need this as we need to export module name for angular DI
	.name;