'use strict';

import {CONST} from 'core/app.constants';

export default class contactsController {
  constructor($http, $scope, $timeout, $rootScope, Notification, pageData) {
      this.CONST = CONST;
      this.map_latitude = pageData.acf.map_latitude;
      this.map_longitude = pageData.acf.map_longitude;
      this.model = {
          contactsInfoTitle: pageData.acf.contacts_info_title,
          contactsInfoSubtitle: pageData.acf.contacts_info_subtitle
      }
      this.googleMapConfig = {
          URL: `https://maps.googleapis.com/maps/api/js?key=${CONST.GOOGLE_MAP_KEY}`,
          zoom: 10,
          styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
      }

      this._$http = $http;
      this._$scope = $scope;
      this._$timeout = $timeout;
      this._$rootScope = $rootScope;
      this.Notification = Notification;
      this.emailFormatRegex = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
      this.isFormSubmitted = false;

      this.form = {
          merge_name: null,
          merge_email: null,
          merge_phone: null,
          merge_message: null
      }
      this._$rootScope.$broadcast('changeHeaderBackground', {'background' : pageData.acf.header_photo})

  }

    resetUserActions() {
        this.isFormSubmitted = false;
        this.myRecaptchaResponse = null;
        this._$scope.contactForm.$setPristine();
        this.form = {
            merge_name: null,
            merge_email: null,
            merge_phone: null,
            merge_message: null
        };
    }

    sendEmail() {
        this.isFormSubmitted = true;

        if (this._$scope.contactForm.$invalid || !this.myRecaptchaResponse) {
            return false;
        }

        let emailConfig = {
            apikey: CONST.ELASTIC_EMAIL.API_KEY,
            template: CONST.ELASTIC_EMAIL.TEMPLATES.CONTACT_FORM_ID,
            to: CONST.ADMIN_EMAIL.CONTACT,
        };

        let emailData = Object.assign(emailConfig, this.form)

        this._$http(
            {
                method: 'POST',
                url: CONST.ELASTIC_EMAIL.API,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: obj => {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: emailData
            }
        ).then(res => {
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.success('You have successfully sent message');
        }).catch(err => {
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.error(err.data || 'Error occured');
        })

    }
}