'use strict';

import template from './team.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('team', {
      url: '/team',
      template: template,
      controller: 'teamController',
      controllerAs: 'teamCtrl',
      pageTitle: 'Команда'
    });
}
