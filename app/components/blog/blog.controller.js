'use strict';

import {CONST} from 'core/app.constants';

export default class blogController {
    constructor($scope, $rootScope, $http, $timeout, pageData, postsData) {
        this._$scope = $scope;
        this._$rootScope = $rootScope;
        this._$http = $http;
        this._$timeout = $timeout;
        this._$rootScope.$broadcast('changeHeaderBackground', {'background' : pageData.acf.header_photo});
        this.posts = postsData.data;
        this.pagesCount = postsData.headers()['x-wp-totalpages'];

        this.pagesShown = 1;
    }

    hasMoreItemsToShow() {
        if (this.posts){
            return this.pagesShown < this.pagesCount;
        }
    };

    showMoreItems() {
        this.pagesShown += 1;
        this.search();
    };

    search(){
        this._$http.get(`${CONST.ENV.API}/posts?page=${this.pagesShown}`).then(res => {
            this.posts = this.posts.concat(res.data);
            this._$timeout(() => {
                this._$scope.$apply();
            })
        }).catch(err => {
            console.error(err);
        });
    }


    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };
}