'use strict';

import {CONST} from 'core/app.constants';
import template from './blog.html';

export default function careers($stateProvider) {
    $stateProvider
        .state('blog', {
            url: '/blog',
            template: template,
            controller: 'blogController',
            controllerAs: 'blogCtrl',
            resolve: {
                pageData: ($state, $stateParams, wpDataRes)=>{
                    return wpDataRes.getData(CONST.WP.BLOG_PAGE_ID).then(data => {
                        /**
                         * Some adjusment for state data
                         * to use in page header
                         */
                        $state.next.pageTitle = data.title.rendered;
                        return data;
                    }).catch(err => {
                    });
                },
                postsData: ($http)=>{
                    return $http.get(`${CONST.ENV.API}/posts?page=1&_embed`).then(res => {
                        return res;
                    }).catch(err => {
                        console.error(err);
                    });
                },
                data: (ngMeta, pageData) => {
                    ngMeta.setTitle(pageData.yoast.title || pageData.title.rendered);
                    ngMeta.setTag('description', pageData.yoast.metadesc);
                }
            },
            meta: {
                disableUpdate: true
            }
        });
}
