'use strict';

import {CONST} from 'core/app.constants';
import template from './post.html';

export default function post($stateProvider) {
    $stateProvider
        .state('post', {
            url: '/blog/:id/:slug',
            template: template,
            controller: 'postController',
            controllerAs: 'postCtrl',
            data: {

            },
            resolve: {
                postData: ($http, $state, $stateParams)=>{
                    let id = $stateParams.id;
                    return $http.get(`${CONST.ENV.API}/posts/${id}?_embed`).then(res => {
                        let post = res.data;
                        /**
                         * Some adjusment for state data
                         * to use in page header
                         */
                        $state.next.pageTitle = post.title.rendered;
                        return post;
                    }).catch(err => {
                        console.error(err);
                    });
                },
                data: (ngMeta, postData) => {
                    ngMeta.setTitle(postData.yoast.title || postData.title.rendered);
                    ngMeta.setTag('description', postData.yoast.metadesc);
                }
            }
        });
}
