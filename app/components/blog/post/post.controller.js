'use strict';

import {CONST} from 'core/app.constants';

export default class postController {
    constructor($scope, $http, $timeout, $rootScope, $location, Notification, postData) {
        this._$http = $http;
        this._$scope = $scope;
        this._$timeout = $timeout;
        this._$location = $location;
        this._$rootScope = $rootScope;
        this.Notification = Notification;
        this.post = postData;
        console.log(this.post);
        this._$rootScope.$broadcast('changeHeaderBackground', {'background' : postData.acf.header_photo});
    }

    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };
}