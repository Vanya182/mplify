import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './blog.routes';
import blogController from './blog.controller';

import postRouting from './post/post.routes';
import postController from './post/post.controller';


export default angular.module('app.blog', [uirouter])
    .config(routing)
    .controller('blogController', blogController)

    .config(postRouting)
    .controller('postController', postController)
    // We need this as we need to export module name for angular DI
    .name;