'use strict';

import {CONST} from 'core/app.constants';
import template from './careers.html';

export default function careers($stateProvider) {
  $stateProvider
    .state('careers', {
      url: '/careers',
      template: template,
      controller: 'careersController',
      controllerAs: 'careersCtrl',
      data: {

      },
    pageTitle: 'Find your dream job',
    resolve: {
        pageData: ($state, $stateParams, wpDataRes)=>{
            let id = $stateParams.id;
            return wpDataRes.getData(CONST.WP.CAREERS_PAGE_ID).then(data => {
                /**
                 * Some adjusment for state data
                 * to use in page header
                 */
                $state.next.pageTitle = data.title.rendered;
                return data;
            }).catch(err => {
            });
        },
        data: (ngMeta, pageData) => {
            ngMeta.setTitle(pageData.yoast.title || pageData.title.rendered);
            ngMeta.setTag('description', pageData.yoast.metadesc);
        },
        careersDataResolved: ($http)=>{
            return $http.get(`${CONST.ENV.API}/careers`).then(res => {
                return careers = res.data;
            }).catch(err => {
                console.error(err);
            });
        }
    },
        meta: {
            disableUpdate: true
        }
    });
}
