'use strict';

import {CONST} from 'core/app.constants';

export default class careersController {
  constructor($scope, $rootScope, $http, $timeout, pageData, careersDataResolved) {
    this.$scope = $scope;
    this._$rootScope = $rootScope;
    this._$http = $http;
    this.$timeout = $timeout;
    this.careers = careersDataResolved;
    this.slickConfig = {
        cyclic: true,
        autoplay: true,
        autoplaySpeed: 3000,
          enabled: true,
          draggable: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            }
        ]
    };
    this.model = {
        ourCulture: {
            title: pageData.acf.our_culture_title,
            content: pageData.acf.our_culture_content
        },
        slider: pageData.acf.slider,
        careersList: {
            title: pageData.acf.careers_list_title,
            content: pageData.acf.careers_list_content
        }
    }
    this._$rootScope.$broadcast('changeHeaderBackground', {'background' : pageData.acf.header_photo})
  }


    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };
}