'use strict';

import {CONST} from 'core/app.constants';
import template from './career.html';

export default function career($stateProvider) {
    $stateProvider
        .state('career', {
            url: '/careers/:id/:slug',
            template: template,
            controller: 'careerController',
            controllerAs: 'careerCtrl',
            data: {

            },
            resolve: {
                careerData: ($http, $state, $stateParams)=>{
                    let id = $stateParams.id;
                    return $http.get(`${CONST.ENV.API}/careers/${id}`).then(res => {
                        let career = res.data;
                        /**
                         * Some adjusment for state data
                         * to use in page header
                         */
                        $state.next.pageTitle = career.title.rendered;
                        return career;
                    }).catch(err => {
                        console.error(err);
                    });
                },
                data: (ngMeta, careerData) => {
                    let title = careerData.yoast.title || careerData.title.rendered;
                    let description = careerData.yoast.metadesc;
                    ngMeta.setTitle(title);
                    ngMeta.setTag('description', description);
                    return {
                        title,
                        description
                    }
                }
            },
            meta: {
                disableUpdate: true
            }
        });
}
