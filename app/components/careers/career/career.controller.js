'use strict';

import {CONST} from 'core/app.constants';

export default class careerController {
    constructor($scope, $http, $timeout, $rootScope, $location, Notification, careerData, data) {
        this.seoMetaData = data;
        console.log(this.seoMetaData);
        this.CONST = CONST;
        this._$http = $http;
        this._$scope = $scope;
        this._$timeout = $timeout;
        this._$location = $location;
        this._$rootScope = $rootScope;
        this.Notification = Notification;
        this.career = careerData;

        this.emailFormatRegex = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        this.form = {};
        this.uploadedFilesLength = null;

        this.loginSuccessful = false;
        this.activeProvider = null;

        this._$rootScope.$on('event:social-sign-in-success', (event, userDetails) => {
            this.loginSuccessful = true;
            this.activeProvider = userDetails.provider;
            this._$timeout(() => {
                this._$scope.$apply();
            });
        });

        this.urlToShare = $location.absUrl();
        this.isFormSubmitted = false;

        this._$rootScope.$broadcast('changeHeaderBackground', {'background': careerData.acf.header_photo})
    }

    fileUpdated($files) {
        this.uploadedFilesLength = $files.length;
    }

    resetFiles() {
        this.uploadedFilesLength = 0;
        this.form.files = null;
    }

    resetUserActions() {
        this.myRecaptchaResponse = null;
        this.isFormSubmitted = false;
        this.resetFiles();
        this._$scope.applyForm.$setPristine();
        this.form = {};
    }

    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };

    apply() {
        this.isFormSubmitted = true;

        if (this._$scope.applyForm.$invalid || !this.loginSuccessful || !this.myRecaptchaResponse || !this.uploadedFilesLength) {
            return false;
        }

        let emailConfig = {
            apikey: CONST.ELASTIC_EMAIL.API_KEY,
            template: CONST.ELASTIC_EMAIL.TEMPLATES.USER_JOB_APPLY_ID,
            to: CONST.ADMIN_EMAIL.JOB,
            merge_career: `Title: ${this.career.title.rendered}, ID: ${this.career.id} `
        };

        let emailData = Object.assign(emailConfig, this.form)

        this._$http(
            {
                method: 'POST',
                url: CONST.ELASTIC_EMAIL.API,
                headers: {'Content-Type': undefined},
                transformRequest: data => {
                    let formData = new FormData();
                    Object.keys(data).forEach(key => {
                        formData.append(key, data[key]);
                    });
                    return formData;
                },
                data: emailData
            }
        ).then(res => {
            console.log(res);
            let userResponse = {
                apikey: CONST.ELASTIC_EMAIL.API_KEY,
                template: CONST.ELASTIC_EMAIL.TEMPLATES.USER_JOB_APPLY_RESPONSE_ID,
                to: this.form.merge_email,
            };
            return this._$http(
                {
                    method: 'POST',
                    url: CONST.ELASTIC_EMAIL.API,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    transformRequest: obj => {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: userResponse
                }
            )
        }).then(res => {
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.success('You have successfully applied for this job');
        }).catch(err => {
            console.log(err);
            this.resetUserActions();
            this._$timeout(() => {
                this._$scope.$apply();
            });
            this.Notification.error(err || 'Error occured');
        })

    }
}