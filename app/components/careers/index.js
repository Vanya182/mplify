import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './careers.routes';
import careersController from './careers.controller';

import singleRouting from './career/career.routes';
import singleCareerController from './career/career.controller';


export default angular.module('app.careers', [uirouter])
	.config(routing)
	.controller('careersController', careersController)

    .config(singleRouting)
    .controller('careerController', singleCareerController)
	// We need this as we need to export module name for angular DI
	.name;