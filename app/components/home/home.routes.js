'use strict';

import template from './home.html';
import {CONST} from 'core/app.constants';

export default function routes($stateProvider) {
  $stateProvider
    .state('home', {
        url: '/',
        template: template,
        controller: 'homeController',
        controllerAs: 'homeCtrl',
        pageTitle: 'Empower your technology Infrastructure',
        resolve: {
            pageData: ($state, $stateParams, wpDataRes)=>{
                let id = $stateParams.id;
                return wpDataRes.getData(CONST.WP.HOME_PAGE_ID).then(data => {
                    /**
                     * Some adjusment for state data
                     * to use in page header
                     */
                    $state.next.pageTitle = data.title;
                    return data;
                }).catch(err => {
                });
            },
            data: (ngMeta, pageData) => {
                ngMeta.setTitle(pageData.yoast.title || pageData.title.rendered);
                ngMeta.setTag('description', pageData.yoast.metadesc);
            }
        },
        meta: {
            disableUpdate: true
        }
    });
}
