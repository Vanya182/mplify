'use strict'

import {CONST} from 'core/app.constants';

export default class HomeController {
  constructor($scope, $rootScope, $http, $timeout, $sce, pageData) {

      this._$scope = $scope;
    this._$rootScope = $rootScope;
    this._$http = $http;
  	this._$timeout = $timeout;
  	this._$http = $http;
    this._$sce = $sce;
  	this.model = {
        whatWeDoSectionTitle: pageData.acf.what_we_do_section_title,
        whatWeDoSectionSubtitle: pageData.acf.what_we_do_section_subtitle,
        whatWeDoSectionDescription: pageData.acf.what_we_do_section_description,
  		whatWeDoItems: pageData.acf.what_we_do_items,

        someOfOurWorksSectionTitle: pageData.acf.some_of_our_works_section_title,
        someOfOurWorksSectionSubtitle: pageData.acf.some_of_our_works_section_subtitle,
        someOfOurWorks: pageData.acf.works
  	}
  	this._$rootScope.$broadcast('changeHeaderBackground', {'background' : pageData.acf.header_photo})
  }

    animateElementIn($el) {
        let animation = $el.data('animation');
        $el.removeClass('not-visible');
        $el.addClass('animated ' + animation);
    };

    animateElementOut($el) {
        let animation = $el.data('animation');
        $el.addClass('not-visible');
        $el.removeClass('animated ' + animation);
    };

  openRequestModal(){
      this._$rootScope.$broadcast('toggleRequestModal');
  }

  openContactModal(){
        this._$rootScope.$broadcast('toggleContactModal');
  }
}

