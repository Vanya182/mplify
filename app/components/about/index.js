import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './about.routes';
import aboutController from './about.controller';

export default angular.module('app.about', [uirouter])
    .config(routing)
    .controller('aboutController', aboutController)
    // We need this as we need to export module name for angular DI
    .name;