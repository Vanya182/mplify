'use strict';

import {CONST} from 'core/app.constants';

export default class servicesController {
    constructor($scope, $rootScope, $timeout, pageData) {
        this._$scope = $scope;
        this._$rootScope = $rootScope;

        this.slickConfig = {
            cyclic: true,
            autoplay: true,
            autoplaySpeed: 3000,
            enabled: true,
            draggable: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                }
            ]
        };

        this.model = {
            sections: pageData.acf.sections,
        }
        this._$rootScope.$broadcast('changeHeaderBackground', {'background' : pageData.acf.header_photo})

    }
}