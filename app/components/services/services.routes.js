'use strict';

import template from './services.html';
import {CONST} from 'core/app.constants';

export default function routes($stateProvider) {
    $stateProvider
        .state('services', {
            url: '/services',
            template: template,
            controller: 'servicesController',
            controllerAs: 'servicesCtrl',
            resolve: {
                pageData: ($state, $stateParams, wpDataRes)=>{
                    let id = $stateParams.id;
                    return wpDataRes.getData(CONST.WP.SERVICES_PAGE_ID).then(data => {
                        /**
                         * Some adjusment for state data
                         * to use in page header
                         */
                        $state.next.pageTitle = data.title.rendered;
                        return data;
                    }).catch(err => {
                    });
                },
                data: (ngMeta, pageData) => {
                    ngMeta.setTitle(pageData.yoast.title || pageData.title.rendered);
                    ngMeta.setTag('description', pageData.yoast.metadesc);
                }
            },
            meta: {
                disableUpdate: true
            }
        });
}
