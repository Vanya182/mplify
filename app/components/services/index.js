import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './services.routes';
import servicesController from './services.controller';

export default angular.module('app.services', [uirouter])
    .config(routing)
    .controller('servicesController', servicesController)
    // We need this as we need to export module name for angular DI
    .name;