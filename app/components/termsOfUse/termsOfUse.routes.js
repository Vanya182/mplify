'use strict';

import template from './termsOfUse.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('termsOfUse', {
      url: '/terms-of-use',
      // Change to import add html-loader
      template: template
    });
}
