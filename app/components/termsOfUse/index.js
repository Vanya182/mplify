import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './termsOfUse.routes';

export default angular.module('app.termsOfUse', [uirouter])
	.config(routing)
	// We need this as we need to export module name for angular DI
	.name;