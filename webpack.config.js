const 
    webpack = require('webpack'),
    path = require('path'),
    postCssConfig = require("./postcss.config.js"),
    ExtractTextPlugin = require ('extract-text-webpack-plugin');

    webpackConfig = {
        context: __dirname,
        entry: {
            bundle: './app/app.js',
            styles: './app/assets/css/index.scss'
        },
        output: {
            filename: '[name].js',
            path: __dirname + '/dist'
        },
        resolve: {
            modules: [path.resolve(__dirname, "app"), "node_modules"],
            descriptionFiles: ["package.json"],
            extensions: [".js", ".json", ".css"]
        },
        devtool: 'source-map',
        devServer: {
            inline: true,
            port: 2222,
            stats: {colors: true}
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: [/node_modules/],
                    loader: "babel-loader",
                    options: { presets: ['es2015'] }
                }, 
                // {
                //     test: /\.css$/,
                //     use: ExtractTextPlugin.extract({
                //       fallback: "style-loader",
                //       use: ["css-loader"]
                //     }),
                // },
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                      fallback: "style-loader",
                      use: ["css-loader", "postcss-loader", "sass-loader"]
                    }),
                },
                // {
                //     test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
                //     use: 'file-loader'
                // },
                {
                  test: /\.(html)$/,
                  use: {
                    loader: 'html-loader',
                    options: {
                      attrs: [':data-src']
                    }
                  }
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin({ filename: 'bundle.css', allChunks: true }),
            new webpack.ProvidePlugin({
            'window.jQuery': 'jquery',
            'windows.&': 'jquery'
            })
        ],
        watch: true
    };

module.exports = webpackConfig;
