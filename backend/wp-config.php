<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mplify');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BJsxTZdeUaXPdH dWJUTWCf#av7<jk5R<ncTA,O#,U:4]R9(X@ZGy^oJ6`<FW(,m');
define('SECURE_AUTH_KEY',  'h(Zl+_XF(.R_gsR!2b%to[C%;^D*vY8^;]e?|goeWDEr1!_K6$qdajRbE`6Q9Ho,');
define('LOGGED_IN_KEY',    'c.#&XH{E%IetK~;_5g|o=(tS^n*v8_:U4%v$Cl}=:=zXv/E.eYcP%%1?:7$ @N9I');
define('NONCE_KEY',        'F3{;+u&u}piu3zJ-fE87N+7B@i(SjRi{huw6#rm,oD+{p+Kh);,fnP9 0JsG=,Hv');
define('AUTH_SALT',        ';Z9,poVR_CvpuE/wKA1DC2:;;iiV>oiN?jFCT_]fG4Hu/w^xi!=2-&5#4WkX0!a]');
define('SECURE_AUTH_SALT', 's5J|A^Ra$_h#U)1D,PG{%MS3t>=_0-LHkQvY2=_F,nABIvm6ik-:sszhx|QA,Rp:');
define('LOGGED_IN_SALT',   '+k}x8|_sNZw`YADL1nc;*zoGJpu0M<(1dFt{xQopH}B=UR.8P,;TOm@dY;` X3Tu');
define('NONCE_SALT',       '!c,NBa!G0OOC]aPiG9(0}>i^ PrIr{>y+$UQ9AN`t{3~6_4*u3#,gY[,b&<s$Ff_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
